<%-- 
    Document   : registro
    Created on : 29-ago-2018, 17:32:02
    Author     : skyve
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registro</title>
         <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
         <link rel="stylesheet" type="text/css" href="css/index.css">
    </head>
    <body>
        <h1>Registro de Usuario</h1>
        <form action="registro" method="post">
            <div class="form-group">
                <label for="exampleInputEmail1">Usuario</label>
                <input type="text" class="form-control"   placeholder="Ingrese Usuario" name="usuario" required="">
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Password</label>
                <input type="password" class="form-control" name="pass" placeholder="Password" required="">
            </div>
            <div class="form-group form-check">
                <input type="checkbox" class="form-check-input" >
                <label class="form-check-label" for="exampleCheck1">Check me out</label>
            </div>
            <button type="submit" class="btn btn-primary">Registrar</button>
        </form>
    </body>
</html>
