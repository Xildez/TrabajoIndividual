/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author skyve
 */
public class conexion {
    String user = "root"; 
    String pass = "";
    String host = "localhost";
    String port = "3306";
    String db = "login";
    String classname = "com.mysql.jdbc.Driver";
    String url = "jdbc:mysql://"+host+":"+port+"/"+db;
    private Connection con ;
    
    public conexion(){
        try {
            Class.forName(classname);
            con = DriverManager.getConnection(url,user,pass);
            
        } catch (ClassNotFoundException e) {
            System.err.println("error");
        } catch(SQLException ex){
            System.err.println("error");
        }                    
    }
            
    public Connection getConnection(){
        return con;
    }
}
