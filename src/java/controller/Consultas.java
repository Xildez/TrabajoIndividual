/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author skyve
 */
public class Consultas extends conexion {
    
    public boolean autenticacion(String usuario, String pass){
        PreparedStatement pst = null;
        ResultSet rs = null;
        
        try {
            String consulta ="select * from usuario where nombreUsuario=? and passUsuario=?";
            pst = getConnection().prepareStatement(consulta);
            pst.setString(1, usuario);
            pst.setString(2, pass);
            
            rs= pst.executeQuery();
            
            if(rs.absolute(1)){
            return true;
            
            }
            
            
        } catch (Exception e) {
            System.err.println("error"+e);
        }finally{
            try {
                if (getConnection()!= null) getConnection().close();
                if (pst !=null) pst.close();
                if (rs !=null) rs.close();
                
            } catch (Exception e) {
                System.err.println("error"+e);
            }
        }
        
        return false;
    }
    public boolean registrar (String usuario, String pass){
        PreparedStatement pst = null;
        
        try {
            String consulta = "insert into usuario (nombreUsuario, passUsuario) values (?,?)";
            pst = getConnection().prepareStatement(consulta);
            pst.setString(1, usuario);
            pst.setString(2, pass);
            
            if (pst.executeUpdate()==1){
                return true;
            }
            
            
            
        } catch (Exception e) {
            System.err.println("error"+e);
        }finally{
            try {
                if (getConnection() !=null) getConnection().close();
                if (pst !=null) pst.close();
                
            } catch (Exception e) {
            System.err.println("error"+e);
            }
        }          
        return false;
    }
    
    
    
   
}
